#!/bin/bash

GENTOOLS="$HOME/Work/Projects/gentools"
WORK_DIR="$HOME/Work/Research/Granules"

cut -d' ' -f 1 $WORK_DIR/FinalAnalysis/read_counts.txt \
  | parallel -j 4 ./2003__sfs.sh {} 0.1

