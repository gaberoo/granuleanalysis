#!/bin/bash
#SBATCH -N 1
#SBATCH --mem=10G
#SBATCH -n 1
#SBATCH -t 8:00:00
#SBATCH -p newnodes
#SBATCH -J trimmomatic

source ./0000_vars

code=$1
phred=$2

OUT_DIR="$DATA_DIR/peared-trimmed"
mkdir -p $OUT_DIR

# Default trim settings
java -jar $TRIM_DIR/trimmomatic-0.36.jar PE \
  -trimlog $OUT_DIR/${code}.log \
  -threads 1 \
  -phred64 \
  $DATA_DIR/peared/${code}.unassembled.forward.fastq.gz \
  $DATA_DIR/peared/${code}.unassembled.reverse.fastq.gz \
  $OUT_DIR/${code}-fwd-paired.fastq.gz \
  $OUT_DIR/${code}-fwd-single.fastq.gz \
  $OUT_DIR/${code}-rev-paired.fastq.gz \
  $OUT_DIR/${code}-rev-single.fastq.gz \
  ILLUMINACLIP:$TRIM_DIR/adapters/NexteraPE-PE.fa:2:30:10 \
  LEADING:3 \
  TRAILING:3 \
  SLIDINGWINDOW:10:20 \
  MINLEN:36

