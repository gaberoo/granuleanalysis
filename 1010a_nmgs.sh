#!/bin/bash

WORK="$HOME/Work/Research/Granules"
WORK_DIR="$WORK/FinalAnalysis"
DATA_DIR="$WORK/data"

cd $DATA_DIR/NMGS

export OMP_NUM_THREADS=10

data=${1:-"data.csv"}
out=${2:-"nmgs"}

$HOME/Work/Projects/NMGS/C/NMGS -in $data -out $out -v -s

