#!/bin/bash

WORK=$HOME/Work/Research/Granules
GENTOOLS="$HOME/Work/Projects/gentools"

ref="$1"
cores=${2:-2}

DATA_DIR="$HOME/Work/Research/Granules/data"
OUT_DIR="$DATA_DIR/Genomes/Accumulibacter"

cat $WORK/FinalAnalysis/keys.txt | \
  parallel -j $cores \
    $GENTOOLS/vcfConsensus -h {} \
    -f $OUT_DIR/${ref}/{}.bcf \
  >| $OUT_DIR/${ref}.consensus.fasta

