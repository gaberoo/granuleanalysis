# Strain-level diversity drives alternative community types in millimeter scale granular biofilms

This repository contains the analysis scripts for the paper. Much of the
computation was performed on a compute cluster (using SLURM), and the
corresponding scripts are contained in the subdirectory `ClusterScripts`.
Analysis that were performed locally are contained in the master directory.
Some intermediate data files are provided here, but many must be generated
from the raw sequencing data that is available at [ENA PRJEB24825](https://www.ebi.ac.uk/ena/data/view/PRJEB24825).

The scripts use the following environmental variables, which should be set in
a file `0000_vars`:

- `PEAR_DIR`: path to PEAR binaries
- `TRIM_DIR`: path to Trimmomatic binaries
- `DATA_DIR`: path to directory containing data

## A) Prepare sequencing read files

### Download sequencing reads

The first step is to download [the sequencing reads](https://www.ebi.ac.uk/ena/data/view/PRJEB24825) from ENA. I am working on providing a script to do this programatically. Until then, you will need manually download the FASTQ files. 

### Quality-trim sequencing reads

Because some of the sequence fragments might potentially be shorter than the combined forward and reverse read length, we first attempt to join the pared-end reads using [PEAR](https://www.h-its.org/en/research/sco/software/#NextGenerationSequencingSequenceAnalysis).

```bash
source ./0000_vars

mkdir -p $DATA_DIR/peared

while read code fwd rev phred
do
  $PEAR_DIR/pear -b 64 -f $DATA_DIR/raw/$fwd -r $DATA_DIR/raw/$rev -o $DATA_DIR/peared/$code
done < $DATA_DIR/raw/pairs.txt
```

We then further quality trim the unassembled reads from the PEAR output using [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic).

```bash
source ./0000_vars

OUT_DIR="$DATA_DIR/peared-trimmed"
mkdir -p $OUT_DIR

# Loop over all samples
while read code fwd rev phred
do
  # Default trim settings
  java -jar $TRIM_DIR/trimmomatic-0.36.jar PE \
    -trimlog $OUT_DIR/${code}.log \
    -threads 1 \
    -phred64 \
    $DATA_DIR/peared/${code}.unassembled.forward.fastq.gz \
    $DATA_DIR/peared/${code}.unassembled.reverse.fastq.gz \
    $OUT_DIR/${code}-fwd-paired.fastq.gz \
    $OUT_DIR/${code}-fwd-single.fastq.gz \
    $OUT_DIR/${code}-rev-paired.fastq.gz \
    $OUT_DIR/${code}-rev-single.fastq.gz \
    ILLUMINACLIP:$TRIM_DIR/adapters/NexteraPE-PE.fa:2:30:10 \
    LEADING:3 \
    TRAILING:3 \
    SLIDINGWINDOW:10:20 \
    MINLEN:36
done < $DATA_DIR/raw/pairs.txt
```

## B) Map reads to reference database

The next step is to map the reads to the reference database. I will not go into detail here about how this database was constructed, but simply supply it as a downloadable file here:

Then, we map all of the sequencing reads to the reference database using [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml). Note, that alternatively you could use alternative alignment programs such as BWA or NCBI MagicBLAST. Bowtie2 can align short reads using two algorithms: an end-to-end alignment or a local alignment. For this dataset, we only found negligible differences between the two, so here we focus on end-to-end alignment.

The standard Bowtie2 parameters expect the short reads to be quasi-direct sequence reads from the reference genomes and are thus quite restrictive. We thus adjust the cutoff parameters to return all best hits, and will post-process the reads later.

Here, I show the code to map all reads locally, but it is strongly advisable to run this on a compute cluster. Note, that this also assumes that `botwie2` and `samtools` are in the system `PATH`.

```bash
OUT_DIR="$DATA_DIR/topGENOMES"
DB="$DATA_DIR/bt2/topGENOMES_GEL"

code=$1
nodes=$2

mkdir -p $OUT_DIR

score="--score-min L,0,-10 -N 1"

while read code fwd rev phred; do
  # Map the reads with bowtie2
  bowtie2 -x $DB -q $score \
    -U $DATA_DIR/peared/${code}.assembled.fastq.gz \
    -1 $DATA_DIR/peared-trimmed/${code}-fwd-paired.fastq.gz \
    -2 $DATA_DIR/peared-trimmed/${code}-rev-paired.fastq.gz \
    -U $DATA_DIR/peared-trimmed/${code}-fwd-single.fastq.gz \
    -U $DATA_DIR/peared-trimmed/${code}-rev-single.fastq.gz \
    -p $nodes -S $OUT_DIR/${code}.sam

  # Convert to BAM format
  samtools view -b $OUT_DIR/${code}.sam > $OUT_DIR/${code}.bam
  rm $OUT_DIR/${code}.sam

  # Sort and index the BAM files
  samtools sort -T $OUT_DIR/tmp_${code} $OUT_DIR/${code}.bam > $OUT_DIR/${code}.sorted.bam
  samtools index $OUT_DIR/${code}.sorted.bam
done < $DATA_DIR/raw/pairs.txt
```

The BAM files are then processed and analyzed in an `R` script.

## C) Accumulibacter analysis

### C1. Extract reads

In addition to assigning reads to genomes in the reference database, we additionally perform a more in depth analysis of the reads that map back to any of the Accumulibacter genomes.

First, we need to extract the reads.
_Note, that the FASTQ files can be [downloaded directly](https://www.dropbox.com/sh/k9biu9qladix882/AAARsIZNxf4bl6yk3LUUBnSxa?dl=0) as well._

```bash
IN_DIR="$DATA_DIR/topGENOMES"
OUT_DIR="$DATA_DIR/Genomes/Accumulibacter"

mkdir -p $OUT_DIR/Reads

while read code fwd rev phred; do
  cat $OUT_DIR/refs.txt | xargs $SAM_DIR/samtools view \
      -b $IN_DIR/${code}.sorted.bam > $OUT_DIR/Reads/${code}.bam

  # extract FASTQ
  samtools bam2fq \
    -0 $OUT_DIR/Reads/${code}-0.fastq \
    -1 $OUT_DIR/Reads/${code}-1.fastq \
    -2 $OUT_DIR/Reads/${code}-2.fastq \
    -s $OUT_DIR/Reads/${code}-s.fastq \
    $OUT_DIR/BAM/${code}.bam

  # combine all reads
  cat $OUT_DIR/Reads/${code}-*.fastq | gzip > $OUT_DIR/Reads/${code}.fastq.gz

  # clean up
  rm $OUT_DIR/Reads/${code}-{0,1,2,s}.fastq
  rm $OUT_DIR/Reads/${code}.bam
done < data/raw/pairs.txt
```

### C2. Build Bowtie2 databases

```bash
DBDIR="$DATA_DIR/Genomes/Accumulibacter"

FILES=$( find $DBDIR -name "*.fasta.gz" )

for file in $FILES; do
  base=$( basename $file .fasta.gz )
  bowtie2-build $file $DATA_DIR/Genomes/Accumulibacter/$base
done
```

Extract a list of all entries for Accumulibacter

```bash
awk '$4 == "Accumulibacter" { print $2 }' \
  $DATA_DIR/topGENOMES_GEL.genera.txt \
  > $DATA_DIR/Genomes/Accumulibacter/refs.txt
```

### C3. Map reads to genomes
