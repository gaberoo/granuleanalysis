# Mantel test with incomplete data

mantel.z <- function(m1,m2,na.rm=FALSE) {
  .m1 <- m1[lower.tri(m1)]
  .m2 <- m2[lower.tri(m2)]
  .n <- sum(!is.na(.m1*.m2))
  sum(.m1*.m2,na.rm=na.rm)/.n
}

mantel.cor <- function(m1,m2,na.rm=FALSE,method="spearman") {
  .m1 <- m1[lower.tri(m1)]
  .m2 <- m2[lower.tri(m2)]
  .keep <- !is.na(.m1*.m2)
  cor(.m1[.keep],.m2[.keep],method="spearman")
}

mantel <- function(d1,d2,exclude=c(),nperm=1000,threads=1,method="spearman",use="everything") {
  .nm <- intersect(rownames(d1),rownames(d2))
  .nm <- .nm[! .nm %in% exclude]
  if (length(.nm) == 0) return(list(cor=NA,null=NULL,p=NA,n=0,nperm=0))
  .d1 <- d1[.nm,.nm]
  .d2 <- d2[.nm,.nm]
  .x1 <- .d1[lower.tri(.d1)]
  .x2 <- .d2[lower.tri(.d2)]
  .scor1 <- cor(.x1,.x2,method=method,use=use)
  .scor0 <- unlist(parallel::mclapply(1:nperm,function(i) {
    ii <- sample(1:nrow(.d1))
    .xx <- .d2[ii,ii][lower.tri(.d2)]
    cor(as.numeric(.x1),as.numeric(.xx),method=method,use=use)
  },mc.cores=threads))
  list(cor=.scor1,null=.scor0,p=1-mean(.scor1 > .scor0),n=length(.nm),nperm=nperm)
}

mantel.dist <- function(dm1,dm2,nperm=1000,na.rm=FALSE,
                        alternative="two.sided",
                        threads=1,method="z",type="cor",
                        exclude=c(),
                        normalize=FALSE)
{
  if (threads > 1) require(parallel)
  alternative <- match.arg(alternative,c("two.sided","less","greater"))
  if (class(dm1) == "dist") {
# convert to matrix
    m1 <- as.matrix(dm1)
    m2 <- as.matrix(dm2)
# get labels from distance matrix
    nm1 <- attr(dm1,"Labels")
    nm2 <- attr(dm2,"Labels")
  } else {
    m1 <- dm1
    m2 <- dm2
    nm1 <- rownames(m1)
    nm2 <- rownames(m2)
  }
# find overlapping labels
  nm <- intersect(nm1,nm2)
  nm <- nm[!nm %in% exclude]
  if (length(nm) == 0) return(list(p=NA))
# extract overlapping labels
  i1 <- match(nm,nm1)
  i2 <- match(nm,nm2)
# reorder/reduce matrix
  m1 <- m1[i1,i1]
  m2 <- m2[i2,i2]
# normalize
  if (normalize) {
    m1 <- m1/mean(m1)
    m2 <- m2/mean(m2)
  }
# perform randomization
  n <- nrow(m2)
  if (type == "cor") {
    z <- mantel.cor(m1,m2,na.rm=na.rm,method=method)
  } else {
    z <- mantel.z(m1,m2,na.rm=na.rm)
  }
  if (threads > 1) {
    null <- parallel::mclapply(1:nperm,function(i) {
      ii <- sample(1:n)
      mantel.z(m1,m2[ii,ii],na.rm=na.rm)
    }, mc.cores=threads)
  } else {
    null <- replicate(nperm,{
      ii <- sample(1:n)
      if (type == "cor") {
        mantel.cor(m1,m2[ii,ii],na.rm=na.rm,method="spearman")
      } else {
        mantel.z(m1,m2[ii,ii],na.rm=na.rm)
      }
    })
  }
  pval <- switch(alternative,
                 two.sided=2*min(sum(null>=z),sum(null<=z)),
                 less=sum(null<=z),
                 greater=sum(null>=z))
  pval <- (pval+1)/(nperm+1)
  list(z.stat=z,p=pval,n=length(nm),alternative=alternative,null=null)
}

mantel.baker <- function(t1,t2,nperm=1000,pb=FALSE,threads=1,
                         alternative="two.sided",exclude=c())
{
  alternative <- match.arg(alternative,c("two.sided","less","greater"))

  .nm <- intersect(t1$tip.label,t2$tip.label)
  .nm <- .nm[!.nm %in% exclude]
  t1 <- drop.tip(t1,t1$tip.label[! t1$tip.label %in% .nm])
  t2 <- drop.tip(t2,t2$tip.label[! t2$tip.label %in% .nm])

  if (is.null(t1) | is.null(t2)) {
    return(list())
  }

  if (! class(t1) == "hclust") {
    if (! is.ultrametric(t1)) {
      t1$edge.length[t1$edge.length==0] <- 0.001
      t1 <- chronopl(t1,lambda=0.1,tol=0)
    }
  }

  if (! class(t2) == "hclust") {
    if (! is.ultrametric(t2)) {
      t2$edge.length[t2$edge.length==0] <- 0.001
      t2 <- chronopl(t2,lambda=0.1,tol=0)
    }
  }

# perform randomization
  z <- dendextend::cor_bakers_gamma(t1,t2)
  if (nperm > 0) {
    if (threads <= 1) {
      if (pb) { .pb <- txtProgressBar(0,nperm,style=3) }
      null <- sapply(1:nperm,function(i) {
        .t2 <- t2
        .t2$tip.label <- sample(.t2$tip.label)
        if (pb) setTxtProgressBar(.pb,i)
        dendextend::cor_bakers_gamma(t1,.t2)
      })
      if (pb) close(.pb)
    } else {
      require("parallel")
      null <- unlist(parallel::mclapply(1:nperm,function(i) {
        .t2 <- t2
        .t2$tip.label <- sample(.t2$tip.label)
        dendextend::cor_bakers_gamma(t1,.t2)
      },mc.cores=threads))
    }
    pval <- switch(alternative,
                   two.sided=2*min(sum(null>=z),sum(null<=z)),
                   less=sum(null<=z),
                   greater=sum(null>=z))
    pval <- (pval+1)/(nperm+1)
    invisible(list(z.stat=z,p=pval,n=length(nm),alternative=alternative,null=null))
  } else {
    return(z)
  }
}


