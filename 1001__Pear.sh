#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 4:00:00
#SBATCH -p newnodes
#SBATCH -J pear
#SBATCH --mem=1G

source ./0000_vars

code=$1
fwd=$2
rev=$3
phred=$4

mkdir -p $DATA_DIR/peared

# Default trim settings
$PEAR_DIR/pear -b 64 -f $DATA_DIR/raw/$fwd -r $DATA_DIR/raw/$rev -o $DATA_DIR/peared/$code

gzip -f $DATA_DIR/peared/$code*

