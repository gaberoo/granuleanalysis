#!/bin/bash

WORK="$HOME/Work/Research/Granules"
WORK_DIR="$WORK/FinalAnalysis"
DATA_DIR="$WORK/data"
GENTOOLS="$HOME/Work/Projects/gentools"

overlap=${1:-100}
export OMP_NUM_THREADS=${2:-1}
bootstrapReps=${3:-100}
mantelReps=${4:-100}

genera="Accumulibacter Azoarcus Acidovorax Competibacter Dechloromonas Nitrosomonas Nitrospira Thauera"

for g1 in $genera; do
  for g2 in $genera; do
    if [[ $g1 < $g2 ]]; then
      echo "$g1 $g2"
      $GENTOOLS/fasta_cor -v -r $bootstrapReps -m $overlap -M $mantelReps \
        --names $WORK_DIR/keys.txt \
        $DATA_DIR/Genomes/$g1/$g1.concat.fasta \
        $DATA_DIR/Genomes/$g2/$g2.concat.fasta \
        >| $DATA_DIR/Genomes/${g1}-${g2}.txt
    fi
  done
done

#while read genus fasta; do
#  echo "Distances for $genus"
#  DBDIR="$DATA_DIR/Genomes/$genus"
#  $GENTOOLS/dna_dist_mem \
#    $DBDIR/$genus.concat.fasta -O p -m $overlap \
#    >| $DBDIR/$genus.concat.dist.txt
#done <<EOF
#EOF


