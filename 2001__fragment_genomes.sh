#!/bin/bash

GENTOOLS=$HOME/Work/Projects/gentools
WORK_DIR=$HOME/Work/Research/Granules
DATA_DIR=$WORK_DIR/data

ref=$1

if [ -f $DATA_DIR/Genomes/Accumulibacter/$ref.fasta.gz ]; then
  gzip -d $DATA_DIR/Genomes/Accumulibacter/$ref.fasta.gz
fi

echo "Fragmenting $ref..."
$GENTOOLS/fastaFrag -n 100000 \
  $DATA_DIR/Genomes/Accumulibacter/$ref.fasta \
  | gzip > $DATA_DIR/Genomes/Accumulibacter/$ref.fragments.fasta.gz


