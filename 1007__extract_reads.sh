#!/bin/bash

WORK=$HOME/Work/Research/Granules
GENTOOLS=$HOME/Work/Projects/gentools
DATA_DIR=$WORK/data

default_cutoff="-1.86"
genus=$1
code=$2
cutoff=${3:-$default_cutoff}

mkdir -p $DATA_DIR/Genomes/$1/Reads

samtools view -b $DATA_DIR/topGENOMES-end2end/${code}.sorted.bam \
  $( cat $DATA_DIR/Genomes/$genus/refs.txt ) \
  > $DATA_DIR/Genomes/$genus/Reads/${code}.bam

$GENTOOLS/bamcnt fastq -c $cutoff \
  $DATA_DIR/Genomes/$genus/Reads/${code}.bam \
  | gzip > $DATA_DIR/Genomes/$genus/Reads/${code}.fastq.gz

