#!/bin/bash

GENTOOLS=$HOME/Work/Projects/gentools
WORK_DIR=$HOME/Work/Research/Granules
DATA_DIR=$WORK_DIR/data

ref=${1:-"UW1"}
refs="UW1 UW2 BA91 BA92 BA93 BA94 SK01 SK02 SK11 SK12"

echo "Concatenating sequences: $ref"

if [[ "$ref" == "UW1" ]]; then
  awk '{ if ($0 !~ /^>(21n_|1_)/ && $2 == "NC_013194.1") { print; getline; print } else { getline; } }' \
    $DATA_DIR/Genomes/Accumulibacter/$ref.consensus.fasta \
    >| $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta
else
  awk '{ if ($0 !~ /^>(21n_|1_)/) { print; getline; print } else { getline; } }' \
    $DATA_DIR/Genomes/Accumulibacter/$ref.consensus.fasta \
    >| $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta
fi

echo "Adding $ref..."

if [[ "$ref" == "UW1" ]]; then
  echo ">UW1" >> $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta
  awk "/^>/ { n++; getline; } n > 1 { exit } { print }" \
    $DATA_DIR/Genomes/Accumulibacter/$ref.fasta \
    >> $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta
else
  awk -v REF=$ref '/^>/ { print ">" REF " " substr($1,2); getline; print }' \
    $DATA_DIR/Genomes/Accumulibacter/$ref.fasta \
    >> $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta
fi

echo "Adding other Refs..."

for r in $refs; do
  if [[ "$r" != "$ref" ]]; then
    cat $DATA_DIR/Genomes/Accumulibacter/$ref/${r}.consensus.fasta \
      >> $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta
  fi
done

echo "Adding Dechloromonas..."

cat $DATA_DIR/Genomes/Accumulibacter/$ref/Dechloromonas.consensus.fasta \
  >> $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta

if [[ "$ref" == "UW1" ]]; then
  $GENTOOLS/dropSites overlap -v -O fasta --minOverlap 3 \
    $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta \
    > $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.overlap.fasta
else
  awk '/^>/ { print substr($1,2) }' $DATA_DIR/Genomes/Accumulibacter/$ref.fasta \
    | parallel -j 10 bash 1009__split.sh {} \
      $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fasta \
      $DATA_DIR/Genomes/Accumulibacter/$ref/$ref.{}.fasta 3

  tmpfile=$( mktemp )
  awk '/^>/ { print substr($1,2) }' ${DATA_DIR}/Genomes/Accumulibacter/${ref}.fasta \
    | xargs -I {} echo ${DATA_DIR}/Genomes/Accumulibacter/${ref}/${ref}.{}.fasta \
    >| $tmpfile
  $GENTOOLS/fasta_concat.rb < $tmpfile > $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.overlap.fasta
  rm $tmpfile
fi


