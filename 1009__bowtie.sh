#!/bin/bash

DATA_DIR="$HOME/Work/Research/Granules/data"

code=$1
genus=$2

DB="${DATA_DIR}/Genomes/$genus/$genus"
OUT_DIR="${DATA_DIR}/Genomes/$genus/map"

mkdir -p $OUT_DIR

bowtie2 -x $DB --no-unal \
  --score-min L,0,-10 -N 1 \
  -U $DATA_DIR/Genomes/$genus/Reads/${code}.fastq.gz \
  -S $OUT_DIR/${code}.sam

samtools sort $OUT_DIR/${code}.sam > $OUT_DIR/${code}.bam
samtools index $OUT_DIR/${code}.bam

rm $OUT_DIR/${code}.sam


