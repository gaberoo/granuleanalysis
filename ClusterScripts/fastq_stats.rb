#!/usr/bin/env ruby

len = 0
cnt = 0

while line = gets
  if line =~ /^@(.*)\s+$/
    seq = gets
    qhead = gets
    qual = gets
    len += seq.length
    cnt += 1
  else
    puts "Error: #{line}"
    exit
  end
end

puts "#{len} #{cnt} #{1.0*len/cnt}"
