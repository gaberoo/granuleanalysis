#!/bin/bash
#SBATCH -t 4:00:00
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p newnodes
#SBATCH -J bt2
#SBATCH --mem=10G

. /etc/profile.d/modules.sh
module load gcc

SAM_DIR="$HOME/Software/samtools"
BCF_DIR="$HOME/Software/bcftools"

DATA_DIR="/nobackup1/gaberoo/Granules"

code=$1
ref=$2

OUT_DIR="$DATA_DIR/Genomes/Accumulibacter/$ref"
DB="$DATA_DIR/Genomes/Accumulibacter/$ref"

#$SAM_DIR/samtools mpileup -Q 0 -Agf ${OUT_DIR}.fasta -t AD \
#  $OUT_DIR/${code}.bam > $OUT_DIR/${code}.bcf
#$BCF_DIR/bcftools index $OUT_DIR/${code}.bcf

$SAM_DIR/samtools mpileup -Q 0 -IAf ${OUT_DIR}.fasta \
  $OUT_DIR/${code}.bam \
  > $OUT_DIR/${code}.pileup


