#!/bin/bash
#SBATCH -t 12:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J bt2
#SBATCH --mem=63G

. /etc/profile.d/modules.sh
module load gcc

BT2_DIR="$HOME/Software/bowtie2"
SAM_DIR="$HOME/Software/samtools"

DATA_DIR="/nobackup1/gaberoo/Granules"
OUT_DIR="$DATA_DIR/topGENOMES"
DB="$DATA_DIR/bt2/topGENOMES_GEL"

code=$1
nodes=$2

$SAM_DIR/samtools sort -@ $nodes -T $OUT_DIR/tmp_${code} $OUT_DIR/${code}.bam > $OUT_DIR/${code}.sorted.bam
$SAM_DIR/samtools index $OUT_DIR/${code}.sorted.bam

#rm $OUT_DIR/${code}.bam



