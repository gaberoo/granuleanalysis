#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH -t 24:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J bt2
#SBATCH --mem=250G
#SBATCH -o data/megahit/map/bt2_%A_%a.out # Standard output

SAMTOOLS_DIR="$HOME/Software/samtools"
BOWTIE2_DIR="$HOME/Software/bowtie2"
DATA_DIR="/nobackup1/gaberoo/Granules"

DB="$DATA_DIR/megahit/granules.contigs"
SEQ_DIR="$DATA_DIR/trimmed"
OUT_DIR="$DATA_DIR/megahit/map"

code=$1
phred=$2

$BOWTIE2_DIR/bowtie2 -x $DB -q --phred${phred} \
  -1 $SEQ_DIR/${code}-fwd-paired.fastq.gz \
  -2 $SEQ_DIR/${code}-rev-paired.fastq.gz \
  -U $SEQ_DIR/${code}-fwd-single.fastq.gz \
  -U $SEQ_DIR/${code}-rev-single.fastq.gz \
  -p 20 -S $OUT_DIR/${code}.sam

$SAMTOOLS_DIR/samtools sort -@ 20 -T $OUT_DIR/tmp_${code} $OUT_DIR/${code}.sam > $OUT_DIR/${code}.sorted.bam

rm $OUT_DIR/${code}.sam


