#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --mem=250G
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J MaxBin
#SBATCH -o /home/gaberoo/Granules/data/megahit/maxbin.out

SAMTOOLS_DIR="$HOME/Software/samtools"
MAXBIN="$HOME/Software/MaxBin-2.2.4"

HMMER=/nobackup1/chisholmlab/software/hmmer3/bin
PRODIGAL=/nobackup1/chisholmlab/software/Prodigal
PPLACER=/nobackup1/chisholmlab/software/pplacer
export PATH=$HMMER:$PRODIGAL:$PPLACER:$PATH

DATA_DIR="/nobackup1/gaberoo/Granules"
ASSEMBLY="$DATA_DIR/megahit"

mkdir -p $ASSEMBLY/coverage

depth="$ASSEMBLY/metabat/depth.txt"

rm -f $ASSEMBLY/coverage/_list
for i in $( seq 4 2 291 ); do
  file=$( head -n 1 $depth | cut -f $i )
  base=$( basename $file .sorted.bam )
  cov_fn="$ASSEMBLY/coverage/${base}.txt"
  cut -f 1,$i $depth | tail -n +2 > $cov_fn
  echo "$cov_fn" >> $ASSEMBLY/coverage/_list
done

OUT_DIR="$ASSEMBLY/maxbin"
mkdir -p $OUT_DIR

$MAXBIN/run_MaxBin.pl \
  -thread 20 \
  -contig $ASSEMBLY/granules.contigs.fa \
  -abund_list $ASSEMBLY/coverage/_list \
  -out $OUT_DIR/bins

CKM_DIR="$OUT_DIR/checkm"
rm -rf $CKM_DIR
checkm lineage_wf -t 20 -x fasta $OUT_DIR $CKM_DIR

mkdir -p "$OUT_DIR/ssu"
checkm ssu_finder -t 20 $ASSEMBLY/granules.contigs.fa -x fasta $OUT_DIR $OUT_DIR/ssu

checkm qa -t 20 -o 2 $CKM_DIR/lineage.ms $CKM_DIR > $OUT_DIR/checkm-summary.txt



