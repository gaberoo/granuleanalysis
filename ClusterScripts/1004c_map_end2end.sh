#!/bin/bash

mkdir -p data/prokPlus-end2end

while read code fwd rev phred; do
  sbatch -N 1 -n 20 -o data/prokPlus-end2end/${code}.out 1004__bowtie.sh $code 1 end2end
done < data/raw/pairs.txt

