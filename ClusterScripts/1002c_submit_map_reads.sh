#!/bin/bash

while read id fn1 fn2 phred; do
  sbatch 1002__map_reads.sh $id $phred
done < data/raw/pairs.txt
