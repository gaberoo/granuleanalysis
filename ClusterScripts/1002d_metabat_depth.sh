#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J metabat
#SBATCH -o data/megahit/metabat/depth.out
#SBATCH --mem=50G

METABAT="$HOME/Software/metabat"

DATA_DIR="/nobackup1/gaberoo/Granules"
ASSEMBLY="$DATA_DIR/megahit"
OUT_DIR="$ASSEMBLY/metabat"

$METABAT/jgi_summarize_bam_contig_depths \
  --outputDepth $OUT_DIR/depth.txt \
  $ASSEMBLY/map/*.bam


