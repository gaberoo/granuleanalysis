#!/bin/bash

while read code fwd rev phred; do
  sbatch -N 1 -n 20 -o data/topGENOMES/${code}.out 1006__bowtie.sh $code 20 end2end
done < data/raw/pairs.txt

