#!/bin/bash
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -J bt2
#SBATCH --mem=48G

. /etc/profile.d/modules.sh
module load gcc

BT2_DIR="$HOME/Software/bowtie2"
SAM_DIR="$HOME/Software/samtools"

DATA_DIR="/nobackup1/gaberoo/Granules"
OUT_DIR="$DATA_DIR/topGENOMES"
DB="$DATA_DIR/bt2/topGENOMES_GEL"

code=$1
nodes=$2

mkdir -p $OUT_DIR

score="--local --score-min G,20,0"

if [[ $3 == "end2end" ]]; then
  score="--score-min L,0,-10 -N 1"
  OUT_DIR="$OUT_DIR-end2end"
  mkdir -p $OUT_DIR
fi

$BT2_DIR/bowtie2 -x $DB -q $score \
  -U $DATA_DIR/peared/${code}.assembled.fastq.gz \
  -1 $DATA_DIR/peared-trimmed/${code}-fwd-paired.fastq.gz \
  -2 $DATA_DIR/peared-trimmed/${code}-rev-paired.fastq.gz \
  -U $DATA_DIR/peared-trimmed/${code}-fwd-single.fastq.gz \
  -U $DATA_DIR/peared-trimmed/${code}-rev-single.fastq.gz \
  -p $nodes -S $OUT_DIR/${code}.sam

$SAM_DIR/samtools view -b $OUT_DIR/${code}.sam > $OUT_DIR/${code}.bam
rm $OUT_DIR/${code}.sam

#$SAM_DIR/samtools sort -@ $nodes -T $OUT_DIR/tmp_${code} $OUT_DIR/${code}.sam > $OUT_DIR/${code}.sorted.bam



