#!/bin/bash
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -J vcfConsensus
#SBATCH --mem=12G

. /etc/profile.d/modules.sh
module load gcc

GENTOOLS="$HOME/Software/gentools"
ref="$1"

rm -f data/Genomes/Accumulibacter/${ref}.consensus.fasta

while read code fwd rev phred; do
  $GENTOOLS/vcfConsensus -h $code -f data/Genomes/Accumulibacter/${ref}/${code}.bcf \
    >> data/Genomes/Accumulibacter/${ref}.consensus.fasta
done < data/raw/pairs.txt


