#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -J multi
#SBATCH --mem=20G

. /etc/profile.d/modules.sh
module load gcc

SAM_DIR="$HOME/Software/samtools"

DATA_DIR="/nobackup1/gaberoo/Granules"

ref=$1

OUT_DIR="$DATA_DIR/Genomes/Accumulibacter/$ref"

rm $OUT_DIR/_multi.txt
while read code fwd rev phred; do
  echo "$OUT_DIR/${code}.bam" >> $OUT_DIR/_multi.txt
done < data/raw/pairs.txt

$SAM_DIR/samtools mpileup -ARg -t AD \
  -b $OUT_DIR/_multi.txt \
  -f ${OUT_DIR}.fasta \
   > $OUT_DIR/_multi.bcf

#ref="$1"
#while read code fwd rev phred; do
#  sbatch -N 1 -n 1 -o data/Genomes/Accumulibacter/${ref}/${code}-samtools.out 1007__samtools.sh $code $ref
#done < data/raw/pairs.txt

