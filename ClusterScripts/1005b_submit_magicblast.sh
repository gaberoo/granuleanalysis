#!/bin/bash

mkdir -p data/magicblast

while read code fwd rev phred; do
  sbatch -o data/magicblast/${code}.out 1005__magicblast.sh $code
done < data/raw/pairs.txt

