#!/bin/bash

ref="$1"

cut -f 1 data/raw/pairs.txt \
  | xargs -n 1 -I {} sbatch -N 1 -n 1 \
  -o data/Genomes/Accumulibacter/${ref}/{}-samtools.out \
  1007__samtools.sh {} $ref

