#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --mem=250G
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J metaSPAdes
#SBATCH -o /home/gaberoo/Granules/slurm-spades.out

SPADES=$HOME/Software/SPAdes/bin/spades.py

mkdir -p data/spades
rm -rf data/spades/*

cd $HOME/Granules/data/raw

join <( sort $HOME/Granules/data/raw/pairs.txt ) <( sort $HOME/Granules/samples.txt ) \
  | cut -d' ' -f 2 \
  | xargs cat > $HOME/Granules/data/spades/lib-fwd.fastq.gz

join <( sort $HOME/Granules/data/raw/pairs.txt ) <( sort $HOME/Granules/samples.txt ) \
  | cut -d' ' -f 3 \
  | xargs cat > $HOME/Granules/data/spades/lib-rev.fastq.gz

cd $HOME/Granules

# run SPAdes
$SPADES -t 20 -m 250 --meta -o $HOME/Granules/data/spades \
  -1 $HOME/Granules/data/spades/lib-fwd.fastq.gz \
  -2 $HOME/Granules/data/spades/lib-rev.fastq.gz

# continue
#$SPA --continue -t 20 -m 250 --meta -1 data/spades/input-1.fastq -2 data/spades/input-2.fastq -o data/spades

