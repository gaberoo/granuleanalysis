#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -J bt2-build
#SBATCH --mem=23GB

BOWTIE2_DIR="$HOME/Software/bowtie2"

$BOWTIE2_DIR/bowtie2-build \
  data/megahit/granules.contigs.fa \
  data/megahit/granules.contigs

