#!/bin/bash

WORK="$HOME/Granules"
WORK_DIR="$HOME/GranuleAnalysis"
DATA_DIR="$WORK/data"
GENTOOLS="$HOME/Software/gentools"

genera="Accumulibacter Azoarcus Acidovorax Competibacter Dechloromonas Nitrosomonas Nitrospira Thauera"

for g1 in $genera; do
  for g2 in $genera; do
    if [[ $g1 < $g2 ]]; then
      echo "$g1 $g2"
      sbatch $WORK/1009__fasta_cor.sh $g1 $g2 10 100 100
      sbatch $WORK/1009__fasta_cor.sh $g1 $g2 100 100 100
      sbatch $WORK/1009__fasta_cor.sh $g1 $g2 1000 100 100
    fi
  done
done


