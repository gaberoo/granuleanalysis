#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -J magicblast
#SBATCH --mem=20G

NUM_THREADS=1
MAGICBLAST="$HOME/Software/ncbi-magicblast/bin/magicblast"
SAMTOOLS="$HOME/Software/samtools/samtools"

DATA_DIR="/nobackup1/gaberoo/Granules"

DB_FILE="$DATA_DIR/blast/prokPlus"

code=$1

mkdir -p data/magicblast

$MAGICBLAST -db $DB_FILE \
  -num_threads $NUM_THREADS \
  -query <( zcat $DATA_DIR/peared/${code}.assembled.fastq.gz $DATA_DIR/peared-trimmed/${code}-*-single.fastq.gz ) \
  -infmt fastq \
  -outfmt sam -out data/magicblast/$code-single.sam

$MAGICBLAST -db $DB_FILE \
  -num_threads $NUM_THREADS \
  -query <( gzip -dc $DATA_DIR/peared-trimmed/${code}-fwd-paired.fastq.gz ) \
  -query_mate <( gzip -dc $DATA_DIR/peared-trimmed/${code}-rev-paired.fastq.gz ) \
  -paired -infmt fastq \
  -outfmt sam -out data/magicblast/$code-paired.sam

$SAMTOOLS view -b data/magicblast/$code-single.sam > data/magicblast/$code-single.bam 
$SAMTOOLS view -b data/magicblast/$code-paired.sam > data/magicblast/$code-paired.bam 

rm data/magicblast/$code-paired.sam
rm data/magicblast/$code-single.sam

