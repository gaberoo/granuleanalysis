#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J MEGAHIT
#SBATCH --mem=250GB
#SBATCH -o slurm-megahit.out

MEGAHIT="$HOME/Software/megahit/megahit"

FWD=$( awk '{ print "data/trimmed/" $0 "-fwd-paired.fastq.gz" }' samples.txt | paste -s -d, - )
REV=$( awk '{ print "data/trimmed/" $0 "-rev-paired.fastq.gz" }' samples.txt | paste -s -d, - )
SINGLE=$( awk '{ print "data/trimmed/" $0 "-fwd-single.fastq.gz,data/trimmed/" $0 "-rev-single.fastq.gz" }' samples.txt | paste -s -d, - )

rm -rf data/megahit

$MEGAHIT -1 $FWD -2 $REV -r $SINGLE --presets meta-large -t 20 -o data/megahit --out-prefix granules
