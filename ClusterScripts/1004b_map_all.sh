#!/bin/bash

while read code fwd rev phred; do
  sbatch -N 1 -n 20 -o data/prokPlus/${code}.out 1004__bowtie.sh $code 20
done < data/raw/pairs.txt

