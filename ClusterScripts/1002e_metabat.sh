#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J metabat
#SBATCH --mem=250G
#SBATCH -o data/megahit/metabat/metabat.out

METABAT="$HOME/Software/metabat"
HMMER=/nobackup1/chisholmlab/software/hmmer3/bin
PRODIGAL=/nobackup1/chisholmlab/software/Prodigal
PPLACER=/nobackup1/chisholmlab/software/pplacer

export PATH=$HMMER:$PRODIGAL:$PPLACER:$PATH

ASSEMBLY="/nobackup1/gaberoo/Granules/megahit"
OUT_DIR="$ASSEMBLY/metabat"
BIN_DIR="$OUT_DIR/bins"
CKM_DIR="$OUT_DIR/checkm"

mkdir -p $BIN_DIR

$METABAT/metabat2 -t 20 -v \
  -i $ASSEMBLY/granules.contigs.fa \
  -a $OUT_DIR/depth.txt \
  -o $OUT_DIR/bins/bin

rm -rf $CKM_DIR
checkm lineage_wf -t 20 -x fa $BIN_DIR $CKM_DIR

mkdir -p $OUT_DIR/ssu
checkm ssu_finder -t 20 $ASSEMBLY/granules.contigs.fa -x fa $BIN_DIR $OUT_DIR/ssu

