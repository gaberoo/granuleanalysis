#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH -p newnodes
#SBATCH -J bt2
#SBATCH --mem=20GB

. /etc/profile.d/modules.sh
module load intel-tbb-oss/intel64/43_20150424oss

BT2_DIR="$HOME/Software/bowtie2"

DATA_DIR="/nobackup1/gaberoo/Granules"
DBDIR="$DATA_DIR/Genomes/Accumulibacter"

FILES=$( find $DBDIR -name "*.fasta" )

for file in $FILES; do
  base=$( basename $file .fasta )
  $BT2_DIR/bowtie2-build $file $DATA_DIR/Genomes/Accumulibacter/$base
done

awk '$4 == "Accumulibacter" { print $2 }' \
  data/topGENOMES_GEL.genera.txt \
  > $DATA_DIR/Genomes/Accumulibacter/refs.txt
