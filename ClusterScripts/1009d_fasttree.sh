#!/bin/bash

WORK="$HOME/Granules"
WORK_DIR="$HOME/GranuleAnalysis"
DATA_DIR="$WORK/data"
GENTOOLS="$HOME/Software/gentools"

genera="Azoarcus Acidovorax Competibacter Dechloromonas Nitrosomonas Nitrospira Thauera"

for genus in $genera; do
  sbatch -p newnodes -N 1 -n 1 -t 12:00:00 -J FastTree-$genus \
    --mem=8GB --mail-user=gaberoo@mit.edu \
    --wrap "$HOME/Software/FastTree/FastTree \
    -nosupport -pseudo -gamma -nt \
    -log $DATA_DIR/Genomes/$genus/${genus}.concat.FastTree.log \
    -out $DATA_DIR/Genomes/$genus/${genus}.concat.FastTree.tree \
    $DATA_DIR/Genomes/$genus/${genus}.concat.fasta"
done


