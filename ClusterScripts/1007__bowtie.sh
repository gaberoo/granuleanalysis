#!/bin/bash
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -J bt2
#SBATCH --mem=12G

. /etc/profile.d/modules.sh
module load gcc

BT2_DIR="$HOME/Software/bowtie2"
SAM_DIR="$HOME/Software/samtools"

DATA_DIR="/nobackup1/gaberoo/Granules"

code=$1
ref=$2

OUT_DIR="${DATA_DIR}/Genomes/Accumulibacter/${ref}"
DB="$DATA_DIR/Genomes/Accumulibacter/$ref"

mkdir -p $OUT_DIR

$BT2_DIR/bowtie2 -x $DB --no-unal \
  --score-min L,0,-10 \
  -U $DATA_DIR/Genomes/Accumulibacter/Reads/${code}.fastq.gz \
  -S $OUT_DIR/${code}.sam

$SAM_DIR/samtools sort -@ 2 $OUT_DIR/${code}.sam > $OUT_DIR/${code}.bam
$SAM_DIR/samtools index $OUT_DIR/${code}.bam

rm $OUT_DIR/${code}.sam


