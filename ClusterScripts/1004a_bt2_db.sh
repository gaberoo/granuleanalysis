#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J bt2
#SBATCH --mem=250GB
#SBATCH -o data/bt2/prokPlus.out

. /etc/profile.d/modules.sh
module load intel-tbb-oss/intel64/43_20150424oss

BT2_DIR="$HOME/Software/bowtie2"

DATA_DIR="/nobackup1/gaberoo/Granules"

PROK="$HOME/Databases/ref_prok_rep_genomes/ref_prok_rep_genomes.fasta"
ACCU=$( find $DATA_DIR/Genomes/Accumulibacter -regex ".*f.*" | paste -s -d, - )
COMP="$DATA_DIR/Genomes/Competibacter/Competibacter_denitrificans.fasta"

ALL="$PROK,$ACCU,$COMP"

$BT2_DIR/bowtie2-build --threads 20 $ALL $DATA_DIR/bt2/prokPlus
