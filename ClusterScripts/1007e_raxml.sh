#!/bin/bash
#SBATCH -t 48:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -N 1
#SBATCH -n 20
#SBATCH -J raxml
#SBATCH --mem=250G

. /etc/profile.d/modules.sh
module load gcc
#module load engaging/RAxML

RAxML="$HOME/Software/RAxML"
DATA_DIR="/nobackup1/gaberoo/Granules"

ref="$1"

FASTA="$DATA_DIR/Genomes/Accumulibacter/${ref}.consensus.clean.fasta"

$RAxML/raxmlHPC-PTHREADS-AVX -j -U -m GTRGAMMA -p 02139 \
  -s $FASTA -T 20 -n $ref \
  -w $DATA_DIR/Genomes/Accumulibacter \
  -o Dechloromonas

