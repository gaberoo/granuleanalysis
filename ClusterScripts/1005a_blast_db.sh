#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 12:00:00
#SBATCH -p newnodes
#SBATCH -J BlastDB
#SBATCH --mem=20G
#SBATCH -o data/blast/makedb.out

BLAST_DIR="$HOME/Software/ncbi-blast/bin"
MAKEDB="$HOME/Software/ncbi-magicblast/bin/makeblastdb"
MAGICBLAST="$HOME/Software/ncbi-magicblast/bin/magicblast"

DATA_DIR="/nobackup1/gaberoo/Granules"

DB_NAME="RefSeq Prokaryotes + EBPR (Gabriel E Leventhal, 24 Nov 2017)"
DB_FILE="$DATA_DIR/blast/prokPlus"

PROK="$HOME/Databases/ref_prok_rep_genomes/ref_prok_rep_genomes.fasta"
ACCU=$( find $DATA_DIR/Genomes/Accumulibacter -regex ".*f.*" )
COMP="$DATA_DIR/Genomes/Competibacter/Competibacter_denitrificans.fasta"

cat $PROK $ACCU $COMP | $MAKEDB -dbtype nucl -title "$DB_NAME" -out $DB_FILE 

$BLAST_DIR/blastdbcmd \
  -db $DATA_DIR/blast/prokPlus -entry all \
  -outfmt "%o %l %t" \
  > $DATA_DIR/blast/prokPlus.names.txt 
