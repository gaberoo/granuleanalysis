#!/bin/bash
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --mem=250G
#SBATCH -t 02:00:00
#SBATCH -p sched_mit_chisholm
#SBATCH -J MaxBin
#SBATCH -o /home/gaberoo/Granules/data/megahit/maxbin.out

SAMTOOLS_DIR="$HOME/Software/samtools"
MAXBIN="$HOME/Software/MaxBin-2.2.4"

HMMER=/nobackup1/chisholmlab/software/hmmer3/bin
PRODIGAL=/nobackup1/chisholmlab/software/Prodigal
PPLACER=/nobackup1/chisholmlab/software/pplacer
export PATH=$HMMER:$PRODIGAL:$PPLACER:$PATH

DATA_DIR="/nobackup1/gaberoo/Granules"
ASSEMBLY="$DATA_DIR/megahit"

OUT_DIR="$ASSEMBLY/maxbin"
CKM_DIR="$OUT_DIR/checkm"

#rm -rf $CKM_DIR
#checkm lineage_wf -t 20 -x fasta $OUT_DIR $CKM_DIR

#mkdir -p "$OUT_DIR/ssu"
#checkm ssu_finder -t 20 $ASSEMBLY/granules.contigs.fa -x fasta $OUT_DIR $OUT_DIR/ssu

checkm qa -t 20 -o 2 $CKM_DIR/lineage.ms $CKM_DIR > $OUT_DIR/checkm-summary.txt



