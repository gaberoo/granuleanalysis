#!/bin/bash

ref="$1"
mkdir -p "data/Genomes/Accumulibacter/$ref"

while read code fwd rev phred; do
  sbatch -N 1 -n 1 \
    -o data/Genomes/Accumulibacter/${ref}/${code}.out \
    1007__bowtie.sh $code $ref
done < data/raw/pairs.txt

