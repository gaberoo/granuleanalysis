#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH -p newnodes
#SBATCH -J extract
#SBATCH --mem=4GB

SAM_DIR=$HOME/Software/samtools
DATA_DIR="/nobackup1/gaberoo/Granules"

code=$1

$SAM_DIR/samtools sort $DATA_DIR/topGENOMES-end2end/${code}.bam \
  > $DATA_DIR/topGENOMES-end2end/${code}.sorted.bam

$SAM_DIR/samtools index $DATA_DIR/topGENOMES-end2end/${code}.sorted.bam

cat $DATA_DIR/Genomes/Accumulibacter/refs.txt \
  | xargs $SAM_DIR/samtools view \
    -b data/topGENOMES-end2end/${code}.sorted.bam \
    > $DATA_DIR/Genomes/Accumulibacter/Reads/${code}.bam

$SAM_DIR/samtools bam2fq \
  -0 $DATA_DIR/Genomes/Accumulibacter/Reads/${code}-0.fastq \
  -1 $DATA_DIR/Genomes/Accumulibacter/Reads/${code}-1.fastq \
  -2 $DATA_DIR/Genomes/Accumulibacter/Reads/${code}-2.fastq \
  -s $DATA_DIR/Genomes/Accumulibacter/Reads/${code}-s.fastq \
  $DATA_DIR/Genomes/Accumulibacter/Reads/${code}.bam

cat $DATA_DIR/Genomes/Accumulibacter/Reads/${code}-*.fastq | \
  gzip > $DATA_DIR/Genomes/Accumulibacter/Reads/${code}.fastq.gz

rm $DATA_DIR/Genomes/Accumulibacter/Reads/${code}-{0,1,2,s}.fastq
rm $DATA_DIR/Genomes/Accumulibacter/Reads/${code}.bam

