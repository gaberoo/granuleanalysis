#!/bin/bash

GENTOOLS=$HOME/Work/Projects/gentools
WORK_DIR=$HOME/Work/Research/Granules
DATA_DIR=$WORK_DIR/data

ref=$1
ref2=$2
chr=${3:-"NC_013194.1"}

echo "Consensus $ref..."
$GENTOOLS/vcfConsensus -h $ref \
  $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bcf \
  > $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.consensus.fasta

