#!/bin/bash

GENTOOLS="$HOME/Work/Projects/gentools"

chr=$1
fasta=$2
refFasta=$3
output=$4
minOverlap=${5:-2}

tmp=$( mktemp )

echo "$chr $tmp"

awk -v CHR=$chr '$2 == CHR { print; getline; print }' $fasta >| $tmp

perlCmd='while (<>) { if ($_ !~ /^>/) { chomp $_; print "$_"; } } print "\n";'

if [ $refFasta ]; then
  len=$( samtools faidx $refFasta $chr | perl -e "$perlCmd" | wc -m | tr -d ' ' )
  echo ">Ref $chr $len $refFasta" >> $tmp
  samtools faidx $refFasta $chr | perl -e "$perlCmd" >> $tmp
fi

$GENTOOLS/dropSites overlap -v -O fasta --minOverlap $minOverlap $tmp > $output

#rm $tmp


