#!/bin/bash

DATA_DIR="$HOME/Work/Research/Granules/data"

code=$1
ref=$2

OUT_DIR="${DATA_DIR}/Genomes/Accumulibacter/${ref}"
DB="${DATA_DIR}/Genomes/Accumulibacter/$ref"

mkdir -p $OUT_DIR

bowtie2 -x $DB --no-unal \
  -N 1 --score-min L,0,-10 \
  -U $DATA_DIR/Genomes/Accumulibacter/Reads/${code}.fastq.gz \
  -S $OUT_DIR/${code}.sam

samtools sort $OUT_DIR/${code}.sam > $OUT_DIR/${code}.bam
samtools index $OUT_DIR/${code}.bam

rm $OUT_DIR/${code}.sam


