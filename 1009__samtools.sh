#!/bin/bash

DATA_DIR="$HOME/Work/Research/Granules/data"

code=$1
genus=$2

OUT_DIR="$DATA_DIR/Genomes/$genus"
FASTA=$3

samtools mpileup -Q 0 -Agf $FASTA -t AD \
  $OUT_DIR/map/${code}.bam >| $OUT_DIR/map/${code}.bcf
bcftools index $OUT_DIR/map/${code}.bcf

#$SAM_DIR/samtools mpileup -Q 0 -IAf ${OUT_DIR}.fasta \
#  $OUT_DIR/${code}.bam \
#  > $OUT_DIR/${code}.pileup


