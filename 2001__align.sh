#!/bin/bash

GENTOOLS=$HOME/Work/Projects/gentools
WORK_DIR=$HOME/Work/Research/Granules
DATA_DIR=$WORK_DIR/data

ref=$1
ref2=$2

mkdir -p $DATA_DIR/Genomes/Accumulibacter/$ref2

echo "Aligning $ref..."
bowtie2 -p 10 -x $DATA_DIR/Genomes/Accumulibacter/$ref2 \
  -f -N 1 --score-min L,0,-10 --no-unal \
  -U $DATA_DIR/Genomes/Accumulibacter/$ref.fragments.fasta.gz \
  -S $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.sam \
  | tee $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bt2.log

echo "Sorting $ref..."
samtools sort -@ 10 $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.sam \
  -o $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bam
rm $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.sam

echo "Indexing $ref..."
samtools index $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bam

echo "Piling up $ref..."
samtools mpileup -Q 0 -ARg -t AD \
  -f $DATA_DIR/Genomes/Accumulibacter/$ref2.fasta \
  $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bam \
  > $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bcf
bcftools index $DATA_DIR/Genomes/Accumulibacter/$ref2/$ref.bcf


