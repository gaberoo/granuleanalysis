#!/bin/bash

WORK=$HOME/Work/Research/Granules

ref="$1"

cut -d' ' -f 1 $WORK/FinalAnalysis/read_counts.txt | \
  parallel -j 10 bash 1007__samtools.sh {} $ref

