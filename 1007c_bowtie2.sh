#!/bin/bash

WORK=$HOME/Work/Research/Granules

ref="$1"
mkdir -p "data/Genomes/Accumulibacter/$ref"

cut -d' ' -f 1 $WORK/FinalAnalysis/read_counts.txt | \
  parallel -j 10 bash 1007__bowtie.sh {} $ref


