#!/usr/bin/env perl

use warnings;                # make Perl less forgiving
use strict;

my ($depth) = @ARGV;         # read in depth cutoff from command line parameters
$depth = 0 unless $depth;    # set to zero if not supplied

while (my $line = <STDIN>) {       # loop over the standard input (comes from the pipe)
  my @row = split /\s+/, $line;    # split the row at whitespaces

  # initialize hash to store the counts
  my %cnt = ( "A" => 0, "T" => 0, "G" => 0, "C" => 0 ); 

  my $ref = $row[3];         # get the reference nucleotide

  my @alt = split /,/, $row[4];    # split the alternative nucleotides into an array
  my @extra = split /:/, $row[9];  # split the extra PL:AD information
  my @ad = split /,/, $extra[1];   # split the AD information into an array

  my $total = $ad[0];        # extract the count info for the reference nucleotide
  $cnt{$ref} = $ad[0];

  # loop over the alternative nucleotides
  #   In Perl, $#arr gives the index of the last element in the array, not the
  #   array size. So note that we only loop until the before-last element,
  #   thus ignoring the '<*>' entry.
  for (my $i = 0; $i < $#alt; $i++) {
    $cnt{$alt[$i]} = $ad[$i+1];
    $total += $ad[$i+1];
  }

  # print the row if the minimum depth cutoff is satisfied
  if ($total >= $depth) {
    print "$row[0] $row[1] $total $cnt{A} $cnt{T} $cnt{G} $cnt{C}\n";
  }
}


