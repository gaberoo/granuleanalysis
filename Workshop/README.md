# Approaches for Comparative Genomics of Replicate Microbial Communities

## Introduction

The aim of this workshop is to walk through some steps for comparative genomic
analysis of microbial communities. Most microbiome studies use 16S rRNA
sequencing to determine community composition. However, any analysis based on
conserved marker genes will by design only be able to reach a limited level
of genetic resolution such as genera or species. Most microbes---bacteria in
particular---also display a large amount of genetic diversity with genera
or species. This diversity is only accessible through sequencing of whole
genomes.

We have recently completed [a study](https://dx.doi.org/10.1101/280271) in
which we used whole-genome untargeted "shotgun" metagenomic sequencing to
describe the genetic diversity across a large number of replicate microbial
communities. The study focused on millimeter-scale granular microbial biofilms
that make up granular activated sludge.

## Full Analysis Overview

From samples to results follow these steps:

1. Sample preparation
    1. Extract DNA from samples
    2. Prepare libraries and send to sequencing

2. Quality filtering
    1. Pairing of paired-end reads
    2. Trimming of unpaired reads

3. Reference database
    - Semi-unsupervised: _de-novo_ assembly of metagenome assembled genomes
      (MAGs)
    - Supervised: reference database of published genomes

4. Align short sequencing reads to reference database
    1. Construct database for aligner from multi FASTA file
    2. Run aligner (Bowtie2, BWA, MagicBLAST).  
       **Warning: this step is resource intensive and might be run on a compute cluster**

5. Derive community composition (i.e. "OTU table")
    1. Group reference genomes based on criteria (species, genus, family, etc.)
    2. Correct counts for genome length
    3. **Construct relative abundance table**
    4. Perform common analysis (e.g. in R)

6. [Genomic analysis of a target organism group](#markdown-header-6-genomic-analysis-of-accumulibacter)
    1. Extract reads that map to any of the genomes in the group
    2. Get reprensentative genomes
    3. Map reads to each of the genomes
    4. Get diversity at each base position
    5. Convert BCF files to variant tables
    6. Analyze variability within and between genomes
    7. Build consensus genomes
    8. Reconstruct phylogenies

## Required software

- Some Unix, e.g. Linux, macOS, or Cygwin
- [Bowtie2](http://bowtie-bio.sf.net/bowtie2)  
- [samtools](http://www.htslib.org)
- [bcftools](http://www.htslib.org)

These packages are really easy to install with common package managers.

- macOS
    - Homebrew: `brew install bowtie2 samtools bcftools`
- Linux
    - Linuxbrew: `brew install bowtie2 samtools bcftools`
    - Ubuntu: `apt-get install bowtie2 samtools bcftools`
    - CentOS: `yum install bowtie2 samtools bcftools`
- and more...

## 6  Genomic Analysis of Accumulibacter

In this workshop we will focus on the work-flow steps required for building community-level consensus genomes, and then performing comparative analysis across them. Some basic knowledge of a \*nix environment (e.g. Linux, macOS, Cygwin, etc.) is helpful.

### 6.1  Extract reads that map to Accumulibacter

_Note, that the FASTQ files can be [downloaded
directly](https://www.dropbox.com/sh/k9biu9qladix882/AAARsIZNxf4bl6yk3LUUBnSxa?dl=0) as well._

We assume that we have already successfully mapped all the short reads to the
reference database, which includes Accumulibacter. Thus, we can just extract
all those reads that map to any of the Accumulibacter genomes in the reference
database.

First, we need to extract the reads from the BAM files. This is easily done
in two steps using `samtools`. First, we filter the BAM file for only those
reads that match to a reference of interest. Because we are also dealing with
incomplete genomes that are made up of many contigs, we store all the contig
identifiers in a file [refs.txt](/data/Genomes/Accumulibacter/refs.txt). Then,
we can make use of `samtools view` with a reference filter. The standard
command would be `samtools view <refs> <bamfile>`, where `<refs>` can be
multiple references. To read these from the file, we use `xargs`:

```bash
cat refs.txt | xargs samtools view -b input.bam > filtered.bam
```

Then, we can easily convert these filtered BAM files to FASTQ files:.

```bash
# extract reads as FASTQ
samtools bam2fq -0 pe-null.fastq -1 pe-fwd.fastq -2 pe-rev.fastq \
                -s se.fastq filtered.bam

# combine all reads into a single file and zip to save space
cat pe-null.fastq pe-fwd.fastq pe-rev.fastq se.fastq | gzip > all.fastq.gz

# clean up intermediates to save space
rm pe-null.fastq pe-fwd.fastq pe-rev.fastq se.fastq
```

### 6.2  Make databases for representative genomes

_Bowtie2 databases for these references can be [downloaded
directly](https://www.dropbox.com/s/jkqoub4h8d1puoz/bt2.tar.gz?dl=0)._

The goal here is to identify genomic differences between the Accumulibacter
populations in each granule. We do this by aligning all reads that map to
any Accumulibacter genome to each of the references. This in essence creates
an assembly of the Accumulibacter genomes for each granule, using the known
reference(s) as guides.

Because aligners such as Bowtie2 and BWA only report the best hit, rather
than all hits (unlike BLAST), we cannot directly use the mapping output from
the combined databases, but will re-align all short reads to the reference
genomes.

In our Accumulibacter analysis there are ten reference genomes (click to
download):

- UW1 clade:
  [UW1](/data/Genomes/Accumulibacter/UW1.fasta.gz)
- UW2 clade:
  [UW2](/data/Genomes/Accumulibacter/UW2.fasta.gz) |
  [BA92](/data/Genomes/Accumulibacter/BA92.fasta.gz) |
  [BA93](/data/Genomes/Accumulibacter/BA93.fasta.gz)
- BA91 clade:
  [BA91](/data/Genomes/Accumulibacter/BA91.fasta.gz) |
  [SK01](/data/Genomes/Accumulibacter/SK01.fasta.gz) |
  [SK02](/data/Genomes/Accumulibacter/SK02.fasta.gz)
- BA94 clade:
  [BA94](/data/Genomes/Accumulibacter/BA94.fasta.gz) |
  [SK11](/data/Genomes/Accumulibacter/SK11.fasta.gz) |
  [SK12](/data/Genomes/Accumulibacter/SK12.fasta.gz)

However, in this particular dataset, we only find reads that are assigned to
the clades UW1, UW2, and BA91. Hence, we will only focus on the analysis based
on these three clades.

For the three genomes [UW1](/data/Genomes/Accumulibacter/UW1.fasta.gz),
[UW2](/data/Genomes/Accumulibacter/UW2.fasta.gz),
[BA91](/data/Genomes/Accumulibacter/BA91.fasta.gz) we can build Bowtie2 databases as follows:

```bash
bowtie2-build UW1.fasta.gz UW1
```

This will produce six files `UW1.*.bt2`. We then repeat the same thing for UW2
and BA91.

### 6.3  Map reads to the references

With the extracted reads and mapping databases in hand, we can now re-map all
the reads to each of the genomes. For a single sample and reference genome
(e.g. UW1), the commands for Bowtie2 are:

```bash
# map reads
bowtie2 -x UW1 --no-unal --score-min L,0,-10 \
        -U sample.fastq.gz -S sample-mapped.sam

# sort SAM file and covert to BAM file (BAM is basically compressed and better SAM)
samtools sort sample-mapped.sam > sample-mapped.bam

# create index of BAM file
samtools index sample-mapped.bam
```

We use the following Bowtie2 options:

- `-x`: database
- `--no-unal`: do not keep unmapped reads in the output
- `--score-min L,0,-10`: this sets the scoring threshold. These settings _de
  facto_ turns off thresholding (read more in the Bowtie2 manual)
- `-U`: input reads
- `-S`: output SAM file

Generally, it is never a bad idea to have a quick look at the SAM/BAM files, just to get a feel for what is going on. In principle, SAM files are just text format, so you can just 'look' at them. But for BAM files, you need `samtools view` to translate the output. Also, even if you have SAM files, you might want to use `samtools view` so that you don't see the header information of the file.

```bash
samtools view sample-mapped.bam | less -S
```

We further pipe the output to `less -S` so that it is paged (we can scroll through the output), and also so lines aren't wrapped (`-S`). Each line in a SAM file is a read that is mapped (or potentially unmapped) to a sequence in the database. For example,

```
M01072:70:000000000-ABY0Y:1:2113:11871:6432     16      NC_013194.1     115     6       4M2I16M8I5M4I2M1I2M9I6M1I5M9I9M10I20M1I9M2I10M10I118M1D4M1I173M1I14M4I8M        *       0       0
M01072:70:000000000-ABY0Y:1:2118:16790:18149    16      NC_013194.1     115     6       4M2I16M8I5M4I2M1I2M9I6M1I5M9I9M10I20M1I9M2I9M6I8M1I3M3I108M1D8M1I19M    *       0       0
M01072:70:000000000-ABY0Y:1:1106:17993:5564     16      NC_013194.1     226     42      5M5I4M2I3M3I9M3I74M1D4M1I173M1I7M       *       0       0                                            
M01072:70:000000000-ABY0Y:1:1119:7687:20386     16      NC_013194.1     261     42      60M1D4M1I173M1I14M4I7M2I11M4I9M1I19M6I9M6I8M3I4M4I7M8I13M9I15M  *       0       0                    
M01072:70:000000000-ABY0Y:1:1117:10086:21768    16      NC_013194.1     562     42      10M12I15M9I7M9I5M7I7M1I9M26I14M4I200M1I8M       *       0       0                                    
M01072:70:000000000-ABY0Y:1:1119:11507:7961     16      NC_013194.1     572     7       15M9I7M9I5M7I7M1I9M26I14M4I425M *       0       0                                                    
M01072:70:000000000-ABY0Y:1:2112:12529:8840     16      NC_013194.1     601     42      3M3I21M1I4M1D350M       *       0       0                                                            
M01072:70:000000000-ABY0Y:1:2107:7929:6434      16      NC_013194.1     602     42      4M1I3M1I8M15I6M1I8M11I377M      *       0       0                                                    
M01072:70:000000000-ABY0Y:1:1116:16578:13019    16      NC_013194.1     659     42      4M4I4M5I283M    *       0       0                                                                    
M01072:70:000000000-ABY0Y:1:2118:4369:20230     0       NC_013194.1     699     6       17M2I8M2I4M2I5M4I6M5I2M3I5M3I4M5I10M3I14M2I8M25I43M     *       0       0     
```

Without wanting to understand too much of what is going on here, know only
these facts: The first column is the identifier of the read, and the third
column is the reference it mapped to. The main message is: if there is an
entry, then the read mapped somewhere to the reference.


### 6.3  Aggregate mapped reads (i.e. pileup)

Next, we need to first transform the raw mapping into a more useful format.
One of the basic questions will be: "How do SNPs (or SNVs) differ between
granules?". Thus, what we need is to know the distribution of base variants
for each position of the genome. This is called a "pile-up" (you can imagine
piling up reads on top of the reference). Pileups can be computed using
standard tools (such as `samtools`).

```bash
# compute pileup
samtools mpileup -Q 0 -Agf UW1.fasta.gz -t AD sample-mapped.bam > sample-pileup.bcf

# create index
bcftools index ${code}.bcf
```

Let's quickly inspect the output. The top is just header information, but each non-commented line that follows shows the detected bases at each genome position.

```
❯ bcftools view sample-pileup.bcf
##fileformat=VCFv4.2
##FILTER=<ID=PASS,Description="All filters passed">
##samtoolsVersion=1.7+htslib-1.7
...

#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  sample-pileup.bcf
NC_013194.1     1       .       A       C,<*>   0       .       DP=1;I16=0,0,1,0,0,0,0,0,0,0,6,36,0,0,0,0;QS=0,1,0;SGB=-0.379885;MQ0F=0 PL:AD   4,3,0,4,3,4:0,1,0
NC_013194.1     2       .       T       C,<*>   0       .       DP=1;I16=0,0,1,0,0,0,0,0,0,0,6,36,0,0,1,1;QS=0,1,0;SGB=-0.379885;MQ0F=0 PL:AD   4,3,0,4,3,4:0,1,0
NC_013194.1     3       .       T       C,<*>   0       .       DP=1;I16=0,0,1,0,0,0,0,0,0,0,6,36,0,0,2,4;QS=0,1,0;SGB=-0.379885;MQ0F=0 PL:AD   4,3,0,4,3,4:0,1,0
...
NC_013194.1     134     .       A       <*>     0       .       DP=6;I16=6,0,0,0,224,8368,0,0,227,9109,0,0,73,1401,0,0;QS=1,0;MQ0F=0    PL:AD   0,18,137:6,0
NC_013194.1     135     .       G       <*>     0       .       DP=6;I16=6,0,0,0,220,8096,0,0,227,9109,0,0,77,1451,0,0;QS=1,0;MQ0F=0    PL:AD   0,18,137:6,0
NC_013194.1     136     .       T       C,<*>   0       .       DP=6;I16=0,0,6,0,0,0,220,8096,0,0,227,9109,0,0,81,1509;QS=0,1,0;VDB=0.0178597;SGB=-0.616816;MQ0F=0      PL:AD   137,18,0,137,18,137:0,6,0
...
```

If we take for example the last line above, the columns are:

- **CHROM**: NC_013194.1, contig name
- **POS**: 4, base position
- ID: .
- **REF**: T, nucleotide in the reference
- **ALT**: G,\* alternate nucleotides detected (a star is a
  placeholder for empty)
- QUAL: 0
- FILTER: .
- **INFO**: DP=1;I16..., additional information

Here, I've highlighted the interesting columns with respect to this analysis
in bold. What this tells us is that at position 4 of the reference genome,
our sample has alternative nucleotides (in this case G). However, the total
coverage depth (the number of reads that span this genome position is only 1
(DP=1), so it is unclear whether this alternative base is just an error or is
real.

QUAL and FILTER are fields that are used for SNP calling. At this point it is
important to highlight that common SNP callers do not really work as we would
want them to. For one, they assume that the reference is a very good prior for
the *truth*. This need not be the case in our samples, since the reference was
reconstructed from a different data set. This is the main reason why we focus
directly on the SNP profiles.

Finally, the final two columns specify additional information. Here, we have
the fields PL and AD (separated by a :) and then followed by the respective
values. The last line, for example has PL = (137,18,0,137,18,137) and **AD =
(0,6,0)**. This last field is the important one, as is tells how frequent the
different alleles are. For position 136, in this sample we find the nucleotide
`C`, rather than the `T` from the reference.

From this information we can now reconstruct the different distributions of
allele frequencies for each sample.


### 6.4  Convert BCF files to variant tables

Up until now we have only focused on getting our read data in a useful format
for downstream analysis.At the end of the previous section, you may have
noticed that we now pretty much have the information we want, but that it is
still in a somewhat unusable format.

> Actually, this is technically not true. The BCF format is an extremely
> efficient and compact random access storage for such data. If you are so
> inclined, using the [HTSlib](http://www.htslib.org) directly to operate
> on BCF files from C/C++ code will allow you to drastically increase the
> speed at which calculations can be performed. For some examples on how
> to do this, check out my (poorly documented---sorry) tools in the [gentools
> repo](https://www.bitbucket.org/gentools.git).

From here on out is where things get interesting. In order not to waste too much time installing tools that very well might not compile, you can also transform the BCF file to a more usable format with for example a simple Perl script ([download script](vcf2tab.pl)):

> "Perl?! Are you insane?" you ask. Well, perhaps, or at least a programming
> masochist to a certain degree. But Perl is still pretty much the fastest
> scripting language for text processing, and if that's all you are doing also
> easier to code than say Python. The main problem with Perl is that it is not
> opinionated and thus does not impose coding conventions (e.g. Python is
> ultra-opinionated, think indentation). While this allows you to have your
> own style, it also allows for sloppy coding because there are many many
> shortcuts in Perl to save on lines at the expense of readability. However,
> when coded explicitly, Perl is actually quite readable. Oh, and the regular
> expression support is second to nothing.

```perl
my ($depth) = @ARGV;         # read in depth cutoff from command line parameters
$depth = 0 unless $depth;    # set to zero if not supplied

while (my $line = <STDIN>) {       # loop over the standard input (comes from the pipe)
  my @row = split /\s+/, $line;    # split the row at whitespaces

  # initialize hash to store the counts
  my %cnt = ( "A" => 0, "T" => 0, "G" => 0, "C" => 0 ); 

  my $ref = $row[3];         # get the reference nucleotide

  my @alt = split /,/, $row[4];    # split the alternative nucleotides into an array
  my @extra = split /:/, $row[9];  # split the extra PL:AD information
  my @ad = split /,/, $extra[1];   # split the AD information into an array

  my $total = $ad[0];        # extract the count info for the reference nucleotide
  $cnt{$ref} = $ad[0];

  # loop over the alternative nucleotides
  #   In Perl, $#arr gives the index of the last element in the array, not the
  #   array size. So note that we only loop until the before-last element,
  #   thus ignoring the '<*>' entry.
  for (my $i = 0; $i < $#alt; $i++) {
    $cnt{$alt[$i]} = $ad[$i+1];
    $total += $ad[$i+1];
  }

  # print the row if the minimum depth cutoff is satisfied
  if ($total >= $depth) {
    print "$row[0] $row[1] $total $cnt{A} $cnt{T} $cnt{G} $cnt{C}\n";
  }
}
```

The script will parse each line of the BCF file, extract the base counts and
output them in consistent order for each base position. It will also accept a
filter for the minimum depth for which base positions should be output.

Then, we can simply convert the output to a tabular format. Depending
on the size of the pileup, this can take a while. Also, the text
files can get quite large for large genomes. You can also [just
download](https://www.dropbox.com/sh/dc54vf0tpqpak1w/AADJtm1swWdTZ9wwYIjSei20a?dl=0)
the already converted files.

```bash
bcftools view sample-pileup.bcf | perl vcf2tab.pl 3 > sample-pileup.txt

# optionally, you can compress the text file to save space
gzip sample-pileup.txt
```

### 6.5  Analyze variant tables

Now we can start having some fun! With these tabulated SNP tables in hand, we
can start analyzing the data. This part of the analysis I will show how to do
in R ([full script](snp_tab.R)).

First, we need to read the table into memory.

```R
filename <- "sample-pileup.txt.gz"

# optionally, download a SNP table
download.file(destfile=filename,"https://uc0292131014b0641aa7585ca967.dl.dropboxusercontent.com/cd/0/get/APMCzH7fcL3yfzuZzFLLuXcQZkWl511Cjy5eUnm5mrNioW7I3Pn2tWq1XRtje9m1Plyj2taDaJstcHJAEuO1by2ldPoDiZzyPWopOxWTAxaUIuccnF-vVgtV-zttpU3ahZw-KU5UdBe_4nqp6Xkdx1dFclqjqx8fYMFE6GvpjA6hAE48_E8WOu05seY7P59pXbzv77T-GQnK6wIPvUdn7X2L/file?_download_id=29355651972853225754676161100020687152515104895509302898462840803&dl=1")

# read the tabular data from the file
counts <- read.table(filename, stringsAsFactors=FALSE)
names(counts) <- c("contig","pos","total","A","T","G","C")

# check out the table
head(counts)
```

You should see output that looks roughly like this

| contig      | pos | total | A | T | G | C |
|-------------|-----|-------|---|---|---|---|
| NC_013194.1 | 64  |   3   | 0 | 0 | 0 | 3 |
| NC_013194.1 | 65  |   3   | 3 | 0 | 0 | 0 |
| NC_013194.1 | 66  |   3   | 3 | 0 | 0 | 0 |
| NC_013194.1 | 67  |   3   | 0 | 0 | 0 | 3 |
| NC_013194.1 | 68  |   3   | 3 | 0 | 0 | 0 |
| NC_013194.1 | 69  |   3   | 0 | 0 | 3 | 0 |

A first thing you can do is inspect a histogram of the total read coverage per base. I set the breaks manually such that each histogram bin includes only a single value.

```R
breaks <- seq(3,max(counts$total))
hist(counts$total,breaks=breaks,xlim=c(3,24))
```

> One of my other hobbies is to try and make pretty plots in R without using
> ggplot or other plotting packages. The reason for this is that I believe
> that presentation of data is an integral part of doing science, and relying
> too strongly on opinionated templates confines creativity. For example, the
> full [script file](snp_tab.R) contains code that prettifies the standard
> histogram output from R to produce the plot below without having to
> configure a specific template.

![Read coverage histogram](count_hist.png)

The distribution follows something that might be Poisson-like or something
similar. However, there appears to be an enrichment in read at coverage level
3. We could spend more time thinking about what this distribution means, but
I'll leave that to you. Instead, we'll continue by calculating the majority
base frequency at each site.

```R
# get the maximum count for each position
max.cnt <- apply(counts[,c('A','T','G','C')],1,max)

# get the frequency by dividing by the total count
major <- max.cnt / counts$total

# calculate the number of sites that have SNPs (i.e. majority frequency is not equal to 1)
p.snp <- sum(major < 1) / length(major)
message(paste0("fraction of sites with a SNP = ",round(p.snp,4)))

# mark those positions with SNPs and where a majority can be defined
mark <- major > 0.5 & major < 1

# get the minority frequency by assuming only 1 alternate allele
minor <- 1-major[mark]
```

This code should also print the fraction of sites in the genome that have a
least one read that differs from the majority. For this particular granule
we get 3.5%. Some of these might be artifact that stem from sequencing
errors, mapping errors, or other sources. Thus we also should consider the
frequency of the minority variants across sites. For example, we can plot the
complementary cumulative distribution for the frequency of SNPs.

```R
# calculate and plot the complementary cummulative distribution
breaks <- seq(0,0.5,by=0.01)
ccdf <- sapply(breaks, function(b) sum(minor >= b))/length(minor)

plot(breaks,ccdf,type="s",ylim=c(0,1))
```

![complementary cummulative distribution](ccdf.png)

This plot gives us information on the diversity within a single granule. Furthermore, we can try and extract some extra information about the distribution. For example, the maximum minority frequency that encompasses 50% of variant sites (here = 0.155). Or, the fraction of sites with a minority frequency > 30% (here = 14.7%).

```R
# find the minium bin where fewer than 50% of sites have at least this frequency
idx <- min(which(ccdf < 0.5))

# get the frequency value for this bin
0.5*(breaks[idx]+breaks[idx+1])

# similarly, we can find the fraction of sites with a minority frequency of greater than 30%
ccdf[which(breaks==0.3)]

```

So, if we combine that information with the total frequency of variant sites (3.5%), we get an very rough estimate for the fraction of sites that have 'important variation' (here = 0.49%). Hence, the overall diversity within a single granule---while non-negligible---is rather low.

In the paper, we used a measure that is similar to the mean homozygosity to characterize the overall diversity within and between granules.

```R
# calculate diversity
probs <- counts[,4:7] / counts$total
pi.div <- 1-rowSums(probs*probs)

message(paste0("Mean diversity = ",mean(pi.div)))

# plot a histogram of the diversity in the variable sites
hist(1-rowSums(probs*probs)[mark])
```

### 6.7  Build consensus genomes

Now that we understand the amount of diversity within a granule, we can construct a **consensus Accumulibacter genome** for each individual granule. _Note, that you can always construct such a consensus, but the amount of diversity in the granule will put the meaning of this consensus into context._

In principle, constructing the consensus is straightforward from the variant tables we were working with above: For each position, we just need to find the base with the highest frequency. You could for example do this in a simple R command that is very similar to the one we used previously to find the majority frequency.

```R
# get the majority base (integer format)
max.base.id <- apply(counts[,c('A','T','G','C')],1,which.max)

# convert this to ATGC
max.base <- c('A','T','G','C')[max.base.id]

# construct genome 'scaffold' and fill with 'n'
consensus.vec <- rep('n',max(counts$pos))
consensus.vec[counts$pos] <- max.base

# fill with the base information
consensus <- paste(consensus.vec,collapse="")

# write this in FASTA format
cat(paste0(">",filename,"\n",consensus),file="sample.fasta")
```

You could do this in R for each sample, or you can [download this
alignment](/data/Genomes/Accumulibacter/UW1.outgroup.overlap.filtered.fasta.gz
). This alignment additionally has all sites filtered that do not show any
variation across granules. This reduces the memory requirement when tree
building, but also changes the meaning of the reported branch lengths in the
trees.

With such an alignment in hand, we can begin to do all sorts of fun things, but for now we'll just focus on using it to build a phylogeny. There a bunch of different phylogenetic inference tools. Some will make your computer explode if you give them a whole genome alignment, while others will be more approximative. However, it is important to remember that almost all tools assume a clean evolution by descent, and thus are "wrong" when there is any kind of recombination. Nevertheless, lets make some trees. For a pretty good tree, that will only cripple your computer for a little while, use `FastTree`.

> On Homebrew or Linuxbrew, FastTree and raxml-ng are available on the
> brewsci/bio tap. First run `brew tap brewsci/bio`, then `brew install
> fasttree` and/or `brew install raxml-ng`.

```bash
gzip -d UW1.outgroup.overlap.filtered.fasta.gz
FastTree -nt -gtr -pseudo < UW1.outgroup.overlap.filtered.fasta > out.tree

# or, directly from the gzipped file
gzip -dc UW1.outgroup.overlap.filtered.fasta | FastTree -nt -gtr -pseudo > out.tree
```

You can also try to get an even better tree using RAxML (specifically, `raxml-ng`, as it is much simpler to use).

```bash
raxml-ng --msa UW1.outgroup.overlap.filtered.fasta --model GTR+G
```

However, both of these will be very slow, and for sake of simplicity, here we'll be working in R with the `ape` package. First, we'll just load the alignment into memory.

```R
library(ape)

uw1 <- read.dna("UW1.outgroup.overlap.filtered.fasta",format="fasta")

# or
uw1 <- read.dna(pipe("gzip -dc UW1.outgroup.overlap.filtered.fasta.gz"),format="fasta")
```

Now, phylogenies in R are all based on distance matrices. In its simplest form, you can make one like this:

```R
dist.mat <- dist.dna(uw1,pairwise.deletion=TRUE)
```

Note, that we specified `pairwise.deletion`. This is because we have
incomplete coverage for individual granules and would there not be able to get
a distance matrix.

Furthermore, the alignment is quite large, and will likely clog up your
computer. So, for demonstrative purposes we'll only use a subset of sites.

```R
# select a random subset of sites
sites <- sample.int(ncol(uw1),100000)
uw1.sub <- uw1[,sites]

# compute distance matrix
dist.mat <- dist.dna(uw1.sub,pairwise.deletion=TRUE)
```

With a distance matrix in hand, we can use one of the many algorithms to reconstruct the tree. For now, we'll use `njs` ([NJ\*](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-9-166)), as it can handle incomplete distance matrices.

```R
# compute tree using NJ* (allows for missing distances)
tree <- njs(dist.mat)

# root tree
rooted <- root(tree,"Dechloromonas",resolve.root=TRUE)

# plot tree
plot(rooted,show.tip.label=FALSE)
```

You might see some branches that are pointing in the wrong direction. This is an artifact of the way NJ\* deals with missing information. You can force these to be zero, if necessary

```R
rooted$edge.length[rooted$edge.length < 0] <- 0
```

Finally, since we turned off the tip labels, we lost a lot of information about the tree. Now let's add back marker points at the tips for the sequences which are the reference genomes. (There are 142 samples in total, so starting from 143 onwards are the references.)

```R
# calculate the x position of all the nodes in the tree
#    (this includes the internal nodes, but the tips will 
#     be the first ones in the vector)
depth <- node.depth.edgelength(rooted)
height <- node.height(rooted)

# add points to the tree
points(depth[143:Ntip(rooted)],height[143:Ntip(rooted)],pch=21,bg="grey")
```

![UW1 phylogeny](tree.png)
