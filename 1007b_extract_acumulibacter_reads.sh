#!/bin/bash

WORK=$HOME/Work/Research/Granules

cut -d' ' -f 1 $WORK/FinalAnalysis/read_counts.txt | \
  parallel -j 10 bash 1007__extract_reads.sh Accumulibacter {}

