#!/bin/bash

GENTOOLS=$HOME/Work/Projects/gentools
WORK_DIR=$HOME/Work/Research/Granules
DATA_DIR=$WORK_DIR/data

ref=${1:-"UW1"}

#echo "Calculating pairwise distances..."
#$GENTOOLS/dna_dist_mem -v \
#  -f $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.overlap.fasta \
#  >| $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.dist.txt

# Subsample alignments

idx=$( date | md5 )
mkdir -p $DATA_DIR/Genomes/Accumulibacter/subsample/$idx
echo $idx

for ref in UW1 UW2 BA91
do
  $GENTOOLS/dropSites random --keep 2000 -O fasta \
    $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.overlap.fasta \
    >| $DATA_DIR/Genomes/Accumulibacter/subsample/$idx/$ref.subsample.fasta
done

$GENTOOLS/fasta_concat.rb \
  $DATA_DIR/Genomes/Accumulibacter/subsample/$idx/UW1.subsample.fasta \
  $DATA_DIR/Genomes/Accumulibacter/subsample/$idx/UW2.subsample.fasta \
  $DATA_DIR/Genomes/Accumulibacter/subsample/$idx/BA91.subsample.fasta \
  > $DATA_DIR/Genomes/Accumulibacter/subsample/$idx.fasta

# Phylogenies

#fastme -i $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.dist.txt \
#  -n -s -o $DATA_DIR/Genomes/Accumulibacter/$ref.outgroup.fastme.tree

#$GENTOOLS/fasta_to_phylip -O fastme \
#  $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.overlap.fasta \
#  >| $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.overlap.dna

#fastme -i $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.overlap.phylip \
#  -I $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.fastme.log \
#  -n -s -d -T 10 -g -z 19770525 \
#  -b 100 -B $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.fastme.bootstrap.trees \
#  -o $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.fastme.aln.tree

#OMP_NUM_THREADS=10 FastTree -nt -gtr -gamma -pseudo \
#  -log $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.FastTree.log \
#  < $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.overlap.fasta \
#  > $DATA_DIR/Genomes/Accumulibacter/UW1.outgroup.FastTree.tree


