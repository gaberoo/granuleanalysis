#!/bin/bash

#echo "UW1 UW2 BA91 BA92 BA93 BA94 SK01 SK02 SK11 SK12" \
#  | xargs -n 1 ./2001__fragment_genomes.sh

#echo "UW2 BA91 BA92 BA93 BA94 SK01 SK02 SK11 SK12" \
#  | xargs -I {} -n 1 ./2001__align.sh {} UW1

echo "UW1 BA91 BA92 BA93 BA94 SK01 SK02 SK11 SK12" \
  | xargs -I {} -n 1 ./2001__align.sh {} UW2

echo "UW1 UW2 BA92 BA93 BA94 SK01 SK02 SK11 SK12" \
  | xargs -I {} -n 1 ./2001__align.sh {} BA91

