#!/bin/bash

source ./0000_vars

mkdir -p $DATA_DIR/peared-trimmed

while read base fwd rev phred
do
  bash 1001__Trimming.sh $base $phred
done < $DATA_DIR/raw/pairs.txt

