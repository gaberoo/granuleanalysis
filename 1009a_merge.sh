#!/bin/bash

DATA_DIR=$HOME/Work/Research/Granules/data

#Competibacter $DATA_DIR/Genomes/Competibacter/Competibacter_denitrificans.fasta
while read genus fasta; do
  echo "Merging $genus ($fasta)"
  bash 1009__merge.sh $genus $fasta 1
done <<EOF
Azoarcus      $DATA_DIR/Genomes/Azoarcus/Azoarcus_sp.fasta
Acidovorax    $DATA_DIR/Genomes/Acidovorax/Acidovorax_sp.fasta
Dechloromonas $DATA_DIR/Genomes/Dechloromonas/Dechloromonas_aromatica.fasta
Nitrosomonas  $DATA_DIR/Genomes/Nitrosomonas/Nitrosomonas_sp.fasta
Nitrospira    $DATA_DIR/Genomes/Nitrospira/Nitrospira_defluvii.fasta
Thauera       $DATA_DIR/Genomes/Thauera/Thauera_sp.fasta
EOF


