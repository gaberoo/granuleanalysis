#!/bin/bash

DATA_DIR="$HOME/Work/Research/Granules/data"

code=$1
ref=$2

OUT_DIR="$DATA_DIR/Genomes/Accumulibacter/$ref"
DB="$DATA_DIR/Genomes/Accumulibacter/$ref"

samtools mpileup -Q 0 -Agf ${OUT_DIR}.fasta -t AD \
  $OUT_DIR/${code}.bam > $OUT_DIR/${code}.bcf
bcftools index $OUT_DIR/${code}.bcf

#$SAM_DIR/samtools mpileup -Q 0 -IAf ${OUT_DIR}.fasta \
#  $OUT_DIR/${code}.bam \
#  > $OUT_DIR/${code}.pileup


