#!/bin/bash

GENTOOLS="$HOME/Work/Projects/gentools"
WORK_DIR="$HOME/Work/Research/Granules"

id=$1
sigma=$2

echo $id

grep "^$id" $WORK_DIR/data/Genomes/Accumulibacter/UW1.af1 \
  | $GENTOOLS/sfs -s $sigma \
  | gzip \
  >| $WORK_DIR/data/Genomes/Accumulibacter/UW1/${id}.sfs.txt.gz

