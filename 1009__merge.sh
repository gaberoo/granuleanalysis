#!/bin/bash

WORK="$HOME/Work/Research/Granules"
WORK_DIR="$WORK/FinalAnalysis"
DATA_DIR="$WORK/data"
GENTOOLS="$HOME/Work/Projects/gentools"

genus=$1 # "Competibacter"
fasta=$2 # "$DBDIR/Competibacter_denitrificans.fasta"
cores=${3:-1}

DBDIR="$DATA_DIR/Genomes/$genus"

if [ "" ]; then
  #for file in $FILES; do
  #  base=$( basename $file .fasta )
  #  $BT2_DIR/bowtie2-build $file $DATA_DIR/Genomes/Competibacter/$base
  #done

  awk -v GENUS="$genus" '$4 == GENUS { print $2 }' \
    $WORK_DIR/topGENOMES_GEL.genera.txt \
    > $DBDIR/refs.txt

  cut -d' ' -f 1 $WORK_DIR/read_counts.txt | \
    parallel -j $cores bash 1007__extract_reads.sh $genus {}

  cut -d' ' -f 1 $WORK_DIR/read_counts.txt | \
    parallel -j $cores bash 1009__bowtie.sh {} $genus

  cut -d' ' -f 1 $WORK_DIR/read_counts.txt | \
    parallel -j $cores bash 1009__samtools.sh {} $genus $fasta

  cut -d' ' -f 1 $WORK_DIR/read_counts.txt | \
    parallel -j $cores $GENTOOLS/vcfConsensus -h {} \
      -f $DBDIR/map/{}.bcf >| $DBDIR/$genus.consensus.fasta
fi

mkdir -p $DBDIR/chr
awk '/^>/ { print substr($1,2) }' $fasta \
  | xargs -n 1 -I {} bash 1009__split.sh {} $DBDIR/$genus.consensus.fasta $fasta $DBDIR/chr/{}.fasta
#  | parallel -j $cores bash 1009__split.sh {} $DBDIR/$genus.consensus.fasta $fasta $DBDIR/chr/{}.fasta

tmp=$( mktemp )
cat $WORK_DIR/keys.txt >| $tmp
echo "Ref" >> $tmp

find $DBDIR/chr -name "*.fasta" \
  | $GENTOOLS/fasta_concat.rb -f -k $tmp \
  >| $DBDIR/$genus.concat.fasta

rm $tmp

OMP_NUM_THREADS=$cores \
  $GENTOOLS/dna_dist_mem -v \
  $DBDIR/$genus.concat.fasta -O p -m 1000 \
  >| $DBDIR/$genus.concat.dist.txt

#java -jar ~/Projects/PhyDstar/PhyDstar.jar \
#  -i $DBDIR/$genus.concat.dist.txt
#
#if [ -e $DBDIR/$genus.concat.dist.txt_bionj.t ]; then
#  mv $DBDIR/$genus.concat.dist.txt_bionj.t \
#    $DBDIR/$genus.concat.bionj.tree
#fi

