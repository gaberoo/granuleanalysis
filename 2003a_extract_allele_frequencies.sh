#!/bin/bash

GENTOOLS="$HOME/Work/Projects/gentools"
WORK_DIR="$HOME/Work/Research/Granules"

gzip -dc $WORK_DIR/data/Genomes/Accumulibacter/UW1/_multi.pileup.gz \
  | $GENTOOLS/pileupper af1 \
  -n $WORK_DIR/FinalAnalysis/theta_files.txt -Q 30 \
  > $WORK_DIR/data/Genomes/Accumulibacter/UW1.af1

