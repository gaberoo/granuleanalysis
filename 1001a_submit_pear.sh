#!/bin/bash

source ./0000_vars

mkdir -p $DATA_DIR/peared

while read code fwd rev phred
do
  bash 1001__Pear.sh $code $fwd $rev $phred
done < $DATA_DIR/raw/pairs.txt

