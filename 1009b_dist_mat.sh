#!/bin/bash

WORK="$HOME/Work/Research/Granules"
WORK_DIR="$WORK/FinalAnalysis"
DATA_DIR="$WORK/data"
GENTOOLS="$HOME/Work/Projects/gentools"

overlap=${1:-100}
export OMP_NUM_THREADS=${2:-1}

while read genus fasta; do
  echo "Distances for $genus"
  DBDIR="$DATA_DIR/Genomes/$genus"
  $GENTOOLS/dna_dist_mem \
    $DBDIR/$genus.concat.fasta -O p -m $overlap \
    >| $DBDIR/$genus.concat.dist.txt
done <<EOF
Azoarcus      $DATA_DIR/Genomes/Azoarcus/Azoarcus_sp.fasta
Acidovorax    $DATA_DIR/Genomes/Acidovorax/Acidovorax_sp.fasta
Competibacter $DATA_DIR/Genomes/Competibacter/Competibacter_denitrificans.fasta
Dechloromonas $DATA_DIR/Genomes/Dechloromonas/Dechloromonas_aromatica.fasta
Nitrosomonas  $DATA_DIR/Genomes/Nitrosomonas/Nitrosomonas_sp.fasta
Nitrospira    $DATA_DIR/Genomes/Nitrospira/Nitrospira_defluvii.fasta
Thauera       $DATA_DIR/Genomes/Thauera/Thauera_sp.fasta
EOF


